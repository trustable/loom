#!/usr/bin/env python
# Copyright (C) 2014 Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=

import collections
import contextlib
import HTMLParser
import os
import re
import sys
import shutil
import tempfile

import cliapp
import yaml

import mustard
import yarnlib


class UnmarkedUnimplementedError(cliapp.AppException):

    def __init__(self, unimplemented):
        msg = ('The following scenarios have unimplemented steps, '
               'and the scenario is not marked as unimplemented:\n')
        for path in unimplemented:
            msg += ('\t%s\n' % path)
        cliapp.AppException.__init__(self, msg)


class VCritExtractor(HTMLParser.HTMLParser):
    def __init__(self):
        self.vcrits = set()
        HTMLParser.HTMLParser.__init__(self)
    def handle_starttag(self, tag, attrs):
        if tag != 'a':
            return
        attr_dict = dict(attrs)
        if u'href' not in attr_dict:
            return
        href = attr_dict[u'href']
        if href.startswith('verification-criteria#'):
            self.vcrits.add(href[len('verification-criteria#'):])


def lookup_recursive(mapping, *args):
    d = mapping
    for index in args:
        d = d[index]
    return d


class Loom(cliapp.Application):

    # dummy parameter needed because mustardlib was not designed to be
    # used independently of the Mustard web application
    base_url = '' 

    def add_settings(self):
        self.settings.string(['mustard', 'm'],
                             'Mustard repository',
                             metavar='PATH')
        self.settings.string_list(['implementations', 'i'],
                                  'Implementations for scenarios.',
                                  metavar='PATH')
        self.settings.string(['ref', 'r'],
                             'Ref of mustard to verify.',
                             metavar='REF',
                             default='UNCOMMITTED')
        self.settings.string(['loom-verifiable-tag-name'],
                             'Name of the tag to use to identify requirements '
                             'that can be verified by loom',
                             metavar='NODE_PATH',
                             default='states/loom-testable')
        self.settings.string(['loom-unimplemented-tag-name'],
                             'Name of the tag to use to identify '
                             'verification criteria that are currently '
                             'not implemented',
                             metavar='NODE_PATH',
                             default='states/loom-unimplemented')
        self.settings.string(['machine-parsable-output-file', 'O'],
                             'File path to write more detailed status output '
                             'to in a machine-parsable format.',
                             metavar='PATH', default='')

        # Scenario running options
        self.settings.string(['source-path', 's'],
                             'Path to root of source tree of project.',
                             metavar='PATH',
                             default=os.getcwd())
        self.settings.string_list(['shell-library', 'l'],
                                  'Path to yarn shell library.',
                                  metavar='PATH')
        self.settings.string_list(['env', 'e'],
                                  'add NAME=VALUE to the environment '
                                  'when tests are run',
                                  metavar='NAME=VALUE')

    def parse_env(self):
        for option_arg in self.settings['env']:
            if '=' not in option_arg:
                raise cliapp.AppException(
                    '--env argument must contain "=" '
                    'to separate environment variable name and value')
            key, value = option_arg.split('=', 1)
            yield key, value

    def process_args(self, args):
        implementation_files = self.settings['implementations']
        verifiable_tag_name = self.settings['loom-verifiable-tag-name']
        unimplemented_tag_name = self.settings['loom-unimplemented-tag-name']
        repository = mustard.repository.Repository(self,
                                                   self.settings['mustard'])
        ref = self.settings['ref']
        state_cache = mustard.state.Cache("http://0.0.0.0:8080/", repository)
        raw_tree = mustard.rawtree.Tree(state_cache.get(ref))
        tree = mustard.elementtree.Tree(raw_tree)
        
        stories = {path: element for (path, element)
                   in tree.elements.iteritems()
                   if (element.kind == 'requirement' and
                       verifiable_tag_name in element.tags)}
        ok = True
        for path, story in stories.iteritems():
            vcrits = list(self.find_scenarios(story, raw_tree, tree,
                                              unimplemented_tag_name))
            scenarios, implementations = self.find_used_implementations(
                    vcrits, implementation_files)
            yarn_path = os.path.basename(path) + '.yarn'
            with open(yarn_path, 'w') as f:
                self.write_story(path, story, raw_tree, vcrits, f)
                self.write_implementations(implementations, f)

            stats = self.run_story(scenarios, vcrits, implementations)

            if stats['unexpected-success'] or stats['unexpected-failure']:
                ok = False
                yarn_command = ['yarn', cliapp.shell_quote(yarn_path)]
                yarn_command.extend(cliapp.shell_quote('--shell-lib=%s' % l)
                                    for l in self.settings['shell-library'])
                yarn_command.extend(cliapp.shell_quote('--env=%s' % l)
                                    for l in self.settings['env'])
                self.output.write(
                    'Story %s failed, you can run `%s` '
                    'to retry without re-running Loom.\n' %
                        (story.title, ' '.join(yarn_command)))
            if self.settings['machine-parsable-output-file']:
                with open(self.settings['machine-parsable-output-file'], 'a') as f:
                    stats['story'] = story.title
                    yaml.safe_dump(dict(stats), f, explicit_start=True, default_flow_style=False)
        if not ok:
            sys.exit(1)

    @classmethod
    def find_scenarios(cls, story, raw_tree, tree, unimpl_tag):
        with contextlib.closing(VCritExtractor()) as vce:
            vce.feed(story.description)
        for vcrit_path in vce.vcrits:
            vcrit = tree.elements[vcrit_path]
            implemented = unimpl_tag not in vcrit.tags

            # Retrieve body of verification criterion
            vcrit_body = lookup_recursive(
                raw_tree.data, *(vcrit_path.split('/') + ['description']))

            yield (vcrit_path, vcrit, vcrit_body, implemented)

    @classmethod
    def write_story(cls, path, story, raw_tree, vcrits, f):
        f.write('# %s\n\n' % story.title)
        story_text = lookup_recursive(raw_tree.data,
                                      *(path.split('/') + ['description']))
        for vcrit_path, vcrit, vcrit_body, implemented in vcrits:
            # Replace link to vcrit in story with the body text
            pat = re.compile(r'\[[^\]]*\]\(verification-criteria#' +
                             re.escape(vcrit_path) + r'\)',
                             flags=re.MULTILINE|re.DOTALL)
            story_text = pat.sub('## %(title)s\n\n    SCENARIO %(title)s\n\n%(body)s' %
                                 {'title': vcrit.title, 'body': vcrit_body},
                                 story_text)
        f.write(story_text)

    @classmethod
    def write_implementations(cls, used_impls, f):
        f.write('\n# Implementations\n\n')
        for verb in ('ASSUMING', 'GIVEN', 'WHEN', 'THEN', 'FINALLY'):
            for impl in sorted(i for i in used_impls if i.what == verb):
                f.write('    IMPLEMENTS %s %s\n' % (verb, impl.regexp))
                for line in impl.shell.splitlines():
                    f.write('    ' + line + '\n')
                f.write('\n')

    @classmethod
    def find_used_implementations(cls, vcrits, implementations):
        mdparser = yarnlib.MarkdownParser()
        for _, vcrit, body, _ in vcrits:
            assert 'SCENARIO' not in body
            blocks = mdparser.parse_string('\n    SCENARIO %s\n\n' % vcrit.title)
            assert blocks
            blocks = mdparser.parse_string(body)
            assert blocks
        for impl_path in implementations:
            blocks = mdparser.parse_file(impl_path)
            assert blocks

        block_parser = yarnlib.BlockParser()
        block_parser.parse_blocks(mdparser.blocks)

        sv = yarnlib.ScenarioValidator(block_parser.scenarios)
        sv.validate_all()

        step_connector = yarnlib.ScenarioStepConnector(
            block_parser.implementations, missing_step_cb=lambda *x: True)
        implemented_scenarios = step_connector.connect_implementations(
            block_parser.scenarios)

        # Check unimplemented, but not marked
        unmarked = []
        for unimplemented in (s for s in block_parser.scenarios
                              if s not in implemented_scenarios):
            found = [(path, implemented)
                     for path, vcrit, _, implemented in vcrits
                     if unimplemented.name == vcrit.title]
            assert len(found) == 1
            path, marked_implemented = found[0]
            if marked_implemented:
                # Unimplemented, but not marked, raise error
                unmarked.append(path)
            else:
                # Unimplemented and marked so. Steps have no impls,
                # so attach dummy, failing steps.
                for step in unimplemented.steps:
                    # A failed ASSUMING is a passed test, and it should
                    # fail before the FINALLY, so allow those steps to
                    # pass, but the rest fail.
                    if step.what in ('ASSUMING', 'FINALLY'):
                        shell = 'true'
                    else:
                        shell = 'false'
                    step.implementation = yarnlib.Implementation(
                        step.what, re.escape(step.text), shell)
        if unmarked:
            raise UnmarkedUnimplementedError(unmarked)

        used_impls = set(step.implementation
                         for scenario in implemented_scenarios
                         for step in scenario.steps)

        return (block_parser.scenarios, used_impls)

    def run_story(self, scenarios, vcrits, implementations):
        prelude = yarnlib.load_shell_libraries(self.settings['shell-library'])
        srcdir = self.settings['source-path']

        implemented_scenarios = set()
        for _, vcrit, _, implemented in vcrits:
            if not implemented:
                continue
            for scenario in scenarios:
                if scenario.name == vcrit.title:
                    implemented_scenarios.add(scenario)

        scenario_runner = yarnlib.ScenarioRunner(prelude, srcdir,
                                                 self.parse_env())

        classifications = {
            (True,  True):    'expected-success',
            (False, True):  'unexpected-success',
            (True,  False): 'unexpected-failure',
            (False, False):   'expected-failure',
        }
        stats = {}
        for classification in classifications.itervalues():
            stats[classification] = []

        for scenario in scenarios:
            tempdir = tempfile.mkdtemp()
            try:
                datadir = os.path.join(tempdir, 'DATADIR')
                homedir = os.path.join(datadir, 'home')
                os.makedirs(homedir)

                ok = scenario_runner.run_scenario(scenario, datadir, homedir)
                classification = \
                    classifications[scenario in implemented_scenarios, ok]
                stats[classification].append(scenario.name)
            finally:
                shutil.rmtree(tempdir)

        return stats

if __name__ == '__main__':
    Loom().run()

# vim: set ts=4 sw=4 et:
